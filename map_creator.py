import geopandas as gpd
from folium.plugins import MousePosition, FloatImage
import folium
from geopandas.explore import _categorical_legend


def create_map(input_data_actual, input_data_expected, output_filepath, act_geom_type_all, exp_geom_type_all):
    data_actual = gpd.read_file(input_data_actual)
    data_actual['ROW_ID'] = data_actual.index
    data_actual = data_actual[['ROW_ID', 'geometry']]
    data_expected = gpd.read_file(input_data_expected)
    data_expected['ROW_ID'] = data_expected.index
    data_expected = data_expected[['ROW_ID', 'geometry']]

    if act_geom_type_all == 'POINT' == exp_geom_type_all:
        m = data_expected.explore(color='blue', marker_kwds=dict(radius=10, fill=False), name="expected")
        data_actual.explore(m=m, color='green', marker_kwds=dict(radius=5, fill=False), name="actual")
    elif act_geom_type_all == 'LINE' == exp_geom_type_all:
        m = data_expected.explore(color='blue', style_kwds=dict(weight=10), name="expected")
        data_actual.explore(m=m, color='green', style_kwds=dict(weight=5), name="actual")
    elif act_geom_type_all == 'POLYGON' == exp_geom_type_all:
        m = data_expected.explore(color='blue', name="expected")
        data_actual.explore(m=m, color='green', name="actual")
    else:
        raise TypeError(f'Actual: {act_geom_type_all} and Expected: {exp_geom_type_all} geometries are not the same')

    diff_exp_act = data_expected.overlay(data_actual, how='symmetric_difference')
    if not diff_exp_act.empty:
        diff_exp_act.explore(m=m, color='red', name="diff_expected_actual")
        _categorical_legend(
            m,
            title="Legend",
            categories=[
                "expected",
                "actual",
                "diff_expected_actual"],
            colors=['blue', 'green', 'red'])
    else:
        _categorical_legend(
            m,
            title="Legend",
            categories=[
                "expected",
                "actual"],
            colors=['blue', 'green'])


    folium.TileLayer(tiles='Stamen Terrain').add_to(m)
    folium.TileLayer(tiles='Stamen Toner').add_to(m)
    folium.TileLayer(tiles='Stamen Watercolor').add_to(m)
    folium.TileLayer(tiles='CartoDB dark_matter').add_to(m)
    folium.LayerControl().add_to(m)
    FloatImage(r'north_arrow.jpg', bottom=100, left=0, position='relative').add_to(m)

    formatter_lat = "function(num) {return 'LAT : ' + L.Util.formatNum(num, 4) + ' ';};"
    formatter_lon = "function(num) {return 'LON : ' + L.Util.formatNum(num, 4) + ' ';};"

    MousePosition(
        position="bottomleft",
        separator=" | ",
        empty_string="NaN",
        lng_first=True,
        num_digits=20,
        prefix='Coordinates | ',
        lat_formatter=formatter_lat,
        lng_formatter=formatter_lon,
    ).add_to(m)

    m.save(output_filepath)
