import geopandas as gpd
from folium.plugins import MousePosition, FloatImage
import folium
from geopandas.explore import _categorical_legend

input_data_actual = gpd.read_file(r'data\boundaries_data.geojson')
input_data_expected = gpd.read_file(r'data\boundaries_data.geojson')

m = input_data_expected.explore( color='blue', style_kwds=dict(weight=10), name="expected")
input_data_actual.explore(m=m, color='green', style_kwds=dict(weight=5), name="actual")

diff_exp_act = input_data_expected.overlay(input_data_actual, how='symmetric_difference')
if not diff_exp_act.empty:
    diff_exp_act.explore(m=m, color='red', name="diff_expected_actual")
    _categorical_legend(
        m,
        title="Legend",
        categories=[
            "expected",
            "actual",
            "diff_expected_actual"],
        colors=['blue', 'green', 'red'])
else:
    _categorical_legend(
        m,
        title="Legend",
        categories=[
            "expected",
            "actual"],
        colors=['blue', 'green'])

folium.TileLayer(tiles='Stamen Terrain').add_to(m)
folium.TileLayer(tiles='Stamen Toner').add_to(m)
folium.TileLayer(tiles='Stamen Watercolor').add_to(m)
folium.TileLayer(tiles='CartoDB dark_matter').add_to(m)
folium.LayerControl().add_to(m)
FloatImage(r'img\north_arrow.jpg', bottom=100, left=0, position='relative').add_to(m)

formatter_lat = "function(num) {return 'LAT : ' + L.Util.formatNum(num, 4) + ' ';};"
formatter_lon = "function(num) {return 'LON : ' + L.Util.formatNum(num, 4) + ' ';};"

MousePosition(
    position="bottomleft",
    separator=" | ",
    empty_string="NaN",
    lng_first=True,
    num_digits=20,
    prefix='Coordinates | ',
    lat_formatter=formatter_lat,
    lng_formatter=formatter_lon,
).add_to(m)

m.save('map_line.html')
