from ipyleaflet import Map, Marker, basemaps, basemap_to_tiles

m = Map(
    basemap=basemap_to_tiles(basemaps.OpenStreetMap),
    center=(52.204793, 360.121558),
    zoom=4
)

m.add(Marker(location=(52.204793, 360.121558)))

m.save('map_ipyleaflet.html', title='My Map')
