import geopandas as gpd
# import matplotlib.pyplot as plt
from datetime import datetime

# # Start datetime
# start = datetime.now()

# Read data
# airports = gpd.read_file(r'..\data\ne_10m_airports\ne_10m_airports.shp')
countries = gpd.read_file(r'ne_10m_admin_0_countries\ne_10m_admin_0_countries.shp')
roads = gpd.read_file(r'ne_10m_roads\ne_10m_roads.shp')
regions = gpd.read_file(r'ne_10m_admin_1_states_provinces\ne_10m_admin_1_states_provinces.shp')
cities = gpd.read_file(r'ne_10m_urban_areas\ne_10m_urban_areas.shp')
pubs = gpd.read_file(r'OnTapScrapper\data\shapes\pubs.shp')
breweries = gpd.read_file(r'OnTapScrapper\data\shapes\breweries.shp')

# Select polygon and write data
country_poland = countries[countries['NAME'] == 'Poland']
country_poland.to_file(r'out\country_poland.shp')
regions_poland = regions[regions['admin'] == 'Poland']
regions_poland.to_file(r'out\regions_poland.shp')


# # Select points and write data
# airports_poland = gpd.overlay(airports, country_poland)
# airports_poland.to_file(r'out\airports_poland.shp')

# Select lines and write data
roads_poland = gpd.clip(roads, country_poland)
roads_poland.to_file(r'out\roads_poland.shp')

pubs_poland = gpd.clip(pubs, country_poland)
pubs_poland.to_file(r'out\pubs_poland.shp')

breweries_poland = gpd.clip(breweries, country_poland)
breweries_poland.to_file(r'out\breweries_poland.shp')

cities_poland = gpd.clip(cities, country_poland)
cities_poland.to_file(r'out\cities_poland.shp')

# Change CRS
country_poland_proj = country_poland.to_crs(crs='EPSG:2180')
roads_poland_proj = roads_poland.to_crs(crs='EPSG:2180')
regions_poland_proj = regions_poland.to_crs(crs='EPSG:2180')
pubs_poland_proj = pubs_poland.to_crs(crs='EPSG:2180')
breweries_poland_proj = breweries_poland.to_crs(crs='EPSG:2180')
cities_poland_proj = cities_poland.to_crs(crs='EPSG:2180')
# airports_poland_proj = airports_poland.to_crs(crs='EPSG:2180')

# Add columns and calculate value
country_poland_proj['ROADS_LENGTH_KM'] = int(roads_poland_proj['geometry'].length.sum() / 1000)
# country_poland_proj['APT_NUMBER'] = len(airports_poland.index)

# Write data
country_poland_proj.to_file(r'out\country_poland_proj.shp')
roads_poland_proj.to_file(r'out\roads_poland_proj.shp')
regions_poland_proj.to_file(r'out\regions_poland_proj.shp')
pubs_poland_proj.to_file(r'out\pubs_poland_proj.shp')
breweries_poland_proj.to_file(r'out\breweries_poland_proj.shp')
cities_poland_proj.to_file(r'out\cities_poland_proj.shp')

# Create buffer
roads_poland_proj_buff = roads_poland_proj
roads_poland_proj_buff['geometry'] = roads_poland_proj_buff.geometry.buffer(1000)
roads_poland_proj_buff.to_file(r'out\roads_poland_proj_buff.shp')

pubs_poland_proj_buff = pubs_poland_proj
pubs_poland_proj_buff['geometry'] = pubs_poland_proj_buff.geometry.buffer(1000)
pubs_poland_proj_buff.to_file(r'out\pubs_poland_proj_buff.shp')

breweries_poland_proj_buff = breweries_poland_proj
breweries_poland_proj_buff['geometry'] = breweries_poland_proj_buff.geometry.buffer(10000)
breweries_poland_proj_buff.to_file(r'out\breweries_poland_proj_buff.shp')

# Best place for pub
breweries_cities_pub_region = gpd.clip(cities_poland_proj, breweries_poland_proj_buff)
breweries_cities_roads_pub_region = gpd.clip(roads_poland_proj_buff, breweries_cities_pub_region)
breweries_cities_roads_pubs_pub_region = gpd.overlay(breweries_cities_roads_pub_region, pubs_poland_proj_buff, how='difference')
breweries_cities_roads_pubs_pub_region_diss = breweries_cities_roads_pubs_pub_region.dissolve()
breweries_cities_roads_pubs_pub_region_diss.to_file(r'out\breweries_cities_roads_pubs_pub_region_diss.shp')
breweries_cities_roads_pubs_pub_region_diss.to_file(r'out\breweries_cities_roads_pubs_pub_region_diss.geojson', driver='GeoJSON')


# Erase buffer
# country_poland_proj_erased = gpd.overlay(country_poland_proj, airports_poland_proj_buff, how='difference')
# country_poland_proj_erased.to_file(r'out\country_poland_proj_erased.shp')

# # Map
# axs = plt.axes()
# x_axis = axs.axes.get_xaxis()
# y_axis = axs.axes.get_yaxis()
# x_axis.set_visible(False)
# y_axis.set_visible(False)
#
# roads = roads_poland_proj.plot(ax=axs)
# country = country_poland_proj_erased.plot(ax=roads)
# airports_poland_proj = gpd.read_file(r'out\airports_poland_proj.shp')
# airports = airports_poland_proj.plot(ax=roads)
#
# plt.savefig("map.png")
# plt.show()

# # Stop datetime and print time
# stop = datetime.now()
# print(stop - start)
