import folium
from folium.plugins import MousePosition


m = folium.Map(location=[45.372, -121.6972], zoom_start=12, tiles="Stamen Terrain", control_scale=True)
folium.TileLayer(tiles='OpenStreetMap').add_to(m)

folium.CircleMarker(
    location=[45.3288, -121.6625],
    popup="ID: 123\nElev: Expected",
    color='green',
).add_to(m)

folium.Marker(
    location=[45.3288, -121.6625],
    popup="ID: 123\nElev: Actual",
    icon=folium.Icon(color="red"),
).add_to(m)


folium.LayerControl().add_to(m)

formatter_lat = "function(num) {return 'LAT : ' + L.Util.formatNum(num, 4) + ' ';};"
formatter_lon = "function(num) {return 'LON : ' + L.Util.formatNum(num, 4) + ' ';};"

MousePosition(
    position="bottomright",
    separator=" | ",
    empty_string="NaN",
    lng_first=True,
    num_digits=20,
    prefix='Coordinates | ',
    lat_formatter=formatter_lat,
    lng_formatter=formatter_lon,
).add_to(m)




m.save("map_folium.html")