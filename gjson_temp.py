import geopandas as gpd


cities_data = gpd.read_file(r'data\ne_50m_populated_places\ne_50m_populated_places.shp')
cities_data = cities_data[['NAME', 'ADM0NAME', 'POP_MAX', 'geometry']]
cities_data['GEOM'] = cities_data.geometry.astype(str)
cities_data.to_file(r'data\cities_data.geojson', driver='GeoJSON')

rivers_data = gpd.read_file(r'data\ne_50m_rivers_lake_centerlines\ne_50m_rivers_lake_centerlines.shp')
rivers_data = rivers_data[['name', 'featurecla', 'geometry']]
# rivers_data['GEOM'] = rivers_data.geometry.astype(str)
rivers_data.to_file(r'data\rivers_data.geojson', driver='GeoJSON')

boundaries_data = gpd.read_file(r'data\ne_50m_admin_0_boundary_lines_land\ne_50m_admin_0_boundary_lines_land.shp')
boundaries_data = boundaries_data[['NAME', 'FEATURECLA', 'NE_ID', 'geometry']]
# boundaries_data['GEOM'] = boundaries_data.geometry.astype(str)
boundaries_data['NE_ID'] = boundaries_data['NE_ID'].astype(str)
boundaries_data.to_file(r'data\boundaries_data.geojson', driver='GeoJSON')

countries_data = gpd.read_file(r'data\ne_50m_admin_0_countries\ne_50m_admin_0_countries.shp')
countries_data = countries_data[['ADMIN', 'POP_EST', 'CONTINENT', 'geometry']]
# countries_data['GEOM'] = countries_data.geometry.astype(str)
countries_data.to_file(r'data\countries_data.geojson', driver='GeoJSON')
