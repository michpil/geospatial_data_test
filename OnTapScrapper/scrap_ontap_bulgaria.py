#Data acquisition
#orient on goal first
#it's different, generic systems are hard to implement, design is harder. Specific design is easier.
import json
import os
from datetime import datetime

import requests
from bs4 import BeautifulSoup

from searchapi import get_pub_geo_info_dict, get_brewery_geo_info_dict

root_url = "https://ontap.bg/"

def openLinkAndReturnSoup(url):
    try:
        agent = {
            "User-Agent": 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'}
        page = requests.get(url, headers=agent, timeout=600)
    except:
        return None

    bsObject = BeautifulSoup(page.content, "lxml")

    return bsObject


def get_pubs_info(root_url):
    soup = openLinkAndReturnSoup(root_url)
    x = soup.findAll("table", {"id": "maintable"})[0]
    a= x.findAll("tr")

    url_dicts = []
    urls = []

    hits = 0

    for row in a:
        cells = row.findAll('td')
        if len(cells) > 2:
            venue = cells[-2]
            city = cells[-1].text
            links = venue.findAll("a")
            if len(links) > 0:
                name = links[0].text
                href = links[0]['href']
                lnk = root_url + href

                geoinfo = get_pub_geo_info_dict(name, city)
                if geoinfo:
                    hits += 1
                    print(hits)

                if lnk not in urls:
                    url_dicts.append(
                        {"name": name,
                         "link": lnk,
                         "city": city,
                         "geoinfo": geoinfo}
                    )
                urls.append(lnk)

    return url_dicts


def scrap_pub_site(pub_dict):
    pub_dicts = []
    bhits = 0
    soup = openLinkAndReturnSoup(pub_dict.get('link'))
    x = soup.findAll("table", {"id": "maintable"})[0]
    a = x.findAll("tr")
    for row in a:
        cells = row.findAll('td')
        if len(cells) > 2:
            abv = cells[-2].text
            style = cells[0].text
            name = cells[2].text
            brewery = cells[1].text
            brewery_link = root_url + cells[1].findAll("a")[0]['href']

            if "brewery" not in brewery.lower():
                searchname = brewery + " brewery"
            else:
                searchname = brewery
            geoinfo = get_brewery_geo_info_dict(searchname)
            if geoinfo:
                bhits += 1
                print(bhits)

            pub_dicts.append({"name": name,
                              "style": style,
                              "abv": abv,
                              "brewery": brewery,
                              "brewery_link": brewery_link,
                              "brewery_geoinfo": geoinfo})

    return {pub_dict.get("name"): pub_dicts}


def scrap_pubs(root_url):

    pubs = get_pubs_info(root_url)

    pubs_beer_dicts = []
    brw_urls = []
    for pb_dct in pubs:
        # pub_link = link.get("link")
        pubs_beer_dicts.append(scrap_pub_site(pb_dct))

    return pubs_beer_dicts


def scrap_brewery_site(brewery_info_dict):
    brewery_beer_dict = []
    soup = openLinkAndReturnSoup(brewery_info_dict.get("brewery_link"))
    x = soup.findAll("table", {"id": "maintable"})[0]
    a = x.findAll("tr")
    for row in a:
        cells = row.findAll('td')
        if len(cells) > 2:
            abv = cells[-3].text
            style = cells[1].text
            name = cells[0].text
            venue = cells[-1].text
            venue_link = root_url + cells[-1].findAll("a")[0]['href']
            brewery_beer_dict.append({"name": name,
                                      "style": style,
                                      "abv": abv,
                                      "pub_name": venue,
                                      "pub_link": venue_link})
    return {brewery_info_dict.get('brewery'): brewery_beer_dict}


def scrap_breweries(pubs_beer_dicts):

    brewery_dicts = []
    venue_urls = []

    for pub_dict in pubs_beer_dicts:
        print(pub_dict)
        for beerslistelement in pub_dict.values():
            for beer_dct in beerslistelement:
                brewery_dicts.append(scrap_brewery_site(beer_dct))

    return brewery_dicts


def get_beer_data(root_url):
    pubs = get_pubs_info(root_url)
    pubs_beers = scrap_pubs(root_url)
    breweries = scrap_breweries(pubs_beers)
    top_record = {"pubs": pubs, "pubs_beers": pubs_beers, "breweries_beers": breweries}
    return top_record



def write_to_json(record_dict):
    file_name = "bulgaria_newjson" + datetime.now().strftime("%Y%m%d %H%M%S") + ".json"
    json_str = json.dumps(record_dict, indent=4, ensure_ascii=False).encode('utf8')

    if not os.path.exists("data"):
        os.mkdir("data")

    with open("data/" + file_name, "wb") as json_fl:
        json_fl.write(json_str)

    return "data/" + file_name

beerrec = get_beer_data(root_url)
write_to_json(beerrec)