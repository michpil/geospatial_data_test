import json
import sqlite3

from workondata import get_taps_tuples, get_shop_tuples, get_breweries_tuples, get_beers_tuples


def insert_beers(database_name, beers_list):
    con = sqlite3.connect(database_name)
    cur = con.cursor()
    cur.execute("""CREATE TABLE IF NOT EXISTS beers 
    (beer_name TEXT, 
    made_by TEXT, 
    style TEXT, 
    blg TEXT, 
    abv TEXT, 
    sold_by TEXT) """)

    for beer in beers_list:
        command = """INSERT INTO beers VALUES %s""" % beer
        cur.execute(command)
        con.commit()

    con.close()


def insert_taps(database_name, taps_list):
    con = sqlite3.connect(database_name)
    cur = con.cursor()
    cur.execute("""CREATE TABLE IF NOT EXISTS pubs 
    (city TEXT, 
    name TEXT, 
    lat REAL, 
    lon REAL)""")

    for tap in taps_list:
        command = """INSERT INTO pubs VALUES %s""" % tap
        cur.execute(command)
        con.commit()
    con.close()


def insert_shops(database_name, shops_list):
    con = sqlite3.connect(database_name)
    cur = con.cursor()
    cur.execute("""CREATE TABLE IF NOT EXISTS shops 
    (city TEXT, 
    name TEXT, 
    lat REAL, 
    lon REAL)""")

    for shop in shops_list:
        command = """INSERT INTO shops VALUES %s""" % shop
        cur.execute(command)
        con.commit()
    con.close()


def insert_breweries(database_name, brewries_list):
    con = sqlite3.connect(database_name)
    cur = con.cursor()
    cur.execute("""CREATE TABLE IF NOT EXISTS breweries 
    (city TEXT, 
    name TEXT, 
    lat REAL, 
    lon REAL)""")

    for brewery in brewries_list:
        command = """INSERT INTO breweries VALUES %s""" % brewery
        cur.execute(command)
        con.commit()

    con.close()


def load_json_to_sqlite_db(json_path, database_path):
    with open(json_path, encoding="utf-8") as f:
        jsn = json.load(f)
        taps = get_taps_tuples(jsn)
        shops = get_shop_tuples(jsn)
        brewries = get_breweries_tuples(jsn)
        beers = get_beers_tuples(jsn)

        insert_taps(database_path, taps)
        insert_shops(database_path, shops)
        insert_breweries(database_path, brewries)
        insert_beers(database_path, beers)

    return database_path
