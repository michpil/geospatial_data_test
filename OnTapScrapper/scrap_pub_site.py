import datetime
import json
import os.path

import requests as requests
from bs4 import BeautifulSoup

from searchapi import get_pub_geo_info_dict, get_brewery_geo_info_dict


def openLinkAndReturnSoup(url):
    try:
        agent = {
            "User-Agent": 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'}
        page = requests.get(url, headers=agent, timeout=600)
    except:
        return None

    bsObject = BeautifulSoup(page.content, "lxml")

    return bsObject


def get_cities(root_link):
    cities_with_taps = []
    cities_with_shps = []
    soup = openLinkAndReturnSoup(root_link)
    panel_body = soup.findAll("div", {"class": "panel-body"})
    for panel in panel_body:
        anchors = panel.findAll("a")
        for anch in anchors:
            if anch['href'].endswith("taps"):
                cities_with_taps.append({"city": anch.text.rsplit(" ", 1)[0].strip(), "link": anch['href']})
            elif anch['href'].endswith("shops"):
                cities_with_shps.append({"city": anch.text.rsplit(" ", 1)[0].strip(), "link": anch['href']})
    return cities_with_taps, cities_with_shps


def get_pub_links(city_link):
    pubs = []
    soup = openLinkAndReturnSoup(city_link)
    anchors = soup.findAll("a")
    for anchor in anchors:
        if anchor['href'].endswith('.ontap.pl'):
            pub_name = anchor.parent.text.strip().split("\n")[0].strip()
            pubs.append({"name": pub_name, "link": anchor['href']})

    return pubs


def get_beers(pub_link):
    beers = []
    hits = 0
    rows = openLinkAndReturnSoup(pub_link).findAll("div", {"class": "container-fluid"})
    for row in rows:
        panel = row.findAll("div", {"class": "panel panel-default"})
        for beer in panel:

            brewery_tags = beer.findAll("b", {"class": "brewery"})
            if brewery_tags and brewery_tags[0].text.strip():
                brewery = brewery_tags[0].text.strip()
                brewery_geo_info = get_brewery_geo_info_dict(brewery)
                store_brewery_geoinfo = False
                if brewery_geo_info and brewery_geo_info.get("type") == 'brewery':
                    store_brewery_geoinfo = True
                    hits += 1
            else:
                continue

            panel_text = beer.text.split("\n\n\n")
            beer_name = panel_text[2].split("\t\t")[-1].strip()
            beer_style = panel_text[5].strip()
            if len(panel_text[3].split("·")) > 1:
                beer_blg = panel_text[3].split("·")[0].strip()
                beer_abv = panel_text[3].split("·")[1].strip()
            else:
                beer_blg = ""
                beer_abv = ""

            if len(panel_text[7].split("·"))>1:
                price03 = panel_text[7].split("·")[0].strip()
                price05 = panel_text[7].split("·")[1].strip()
            else:
                price03 = ""
                price05 = ""

            if brewery and beer_name:
                brewery_part = {"name": brewery}
                if store_brewery_geoinfo:
                    brewery_part['geoinfo'] = brewery_geo_info

                beers.append({"brewery": brewery_part,
                              "name": beer_name,
                              "style": beer_style,
                              "blg": beer_blg,
                              "abv": beer_abv,
                              "price_normal": price05,
                              "price_small": price03})

    print(hits)
    return beers


def get_beer_data_from_pubs(cities_with_taps):
    records = []
    for multitap_city in cities_with_taps:
        city = multitap_city.get("city")
        city_link = multitap_city.get('link')
        pubs = get_pub_links(city_link)
        for pub in pubs:
            pub_name = pub.get("name")

            pub_geo_info = get_pub_geo_info_dict(pub_name, city)
            beer_data = get_beers(pub.get("link"))
            record = {"city": city,
                      "pub_name": pub_name,
                      "pub_link": pub.get("link"),
                      "beers": beer_data,
                      "geoinfo": pub_geo_info}
            records.append(record)
    return records


def get_beer_data_from_shops(cities_with_shps):
    records = []
    for shop_city in cities_with_shps:
        city = shop_city.get("city")
        city_link = shop_city.get('link')
        shops = get_pub_links(city_link)
        for shop in shops:
            shop_name = shop.get("name")

            shop_geo_info = get_pub_geo_info_dict(shop_name, city)
            beer_data = get_beers(shop.get("link"))
            record = {"city": city,
                      "shop_name": shop_name,
                      "shop_link": shop.get("link"),
                      "beers": beer_data,
                      "geoinfo": shop_geo_info}
            records.append(record)
    return records


def get_beer_data(root_link):
    cities_with_taps, cities_with_shps = get_cities(root_link)

    multitap_records = get_beer_data_from_pubs(cities_with_taps)
    shop_records = get_beer_data_from_shops(cities_with_shps)

    top_record = {"taps": multitap_records, "shps": shop_records}
    return top_record


def write_to_json(record_dict):
    file_name = "newjson" + datetime.datetime.now().strftime("%Y%m%d %H%M%S") + ".json"
    json_str = json.dumps(record_dict, indent=4, ensure_ascii=False).encode('utf8')

    if not os.path.exists("data"):
        os.mkdir("data")

    with open("data/" + file_name, "wb") as json_fl:
        json_fl.write(json_str)

    return "data/" + file_name
