import requests
import json


def get_pub_geo_info_dict(pub_name, city):
    name_part = pub_name.replace(" ", "+")
    city_part = city.replace(" ", "+")
    lnk = f"https://nominatim.openstreetmap.org/search?q={name_part}+{city_part}&format=json&polygon_geojson=1&addressdetails=1"
    response = requests.get(lnk)
    if response.text != "[]":
        try:
            return json.loads(response.text)[0]
        except:
            return {}
    return {}


def get_brewery_geo_info_dict(brewery):
    brewery_part = brewery.replace(" ", "+")

    lnk = f"https://nominatim.openstreetmap.org/search?q={brewery_part}&format=json&polygon_geojson=1&addressdetails=1"
    response = requests.get(lnk)
    if response.text != "[]":
        return json.loads(response.text)[0]
    return {}
