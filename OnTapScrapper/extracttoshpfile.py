import sqlite3
import pandas as pd
import geopandas
from shapely import LineString


def extract_data_to_points(database_name, table_name, shape_directory):
    con = sqlite3.connect(database_name)
    df = pd.read_sql(f'SELECT * FROM {table_name}', con)
    gdf = geopandas.GeoDataFrame(
        df, geometry=geopandas.points_from_xy(df.lon, df.lat), crs="EPSG:4326"
    )
    gdf.to_file(f'{shape_directory}/{table_name}.shp')


def extract_beer_lines(database_name, shape_directory):
    con = sqlite3.connect(database_name)
    total = pd.read_sql("""SELECT t1.beer_name, t1.made_by, t1.sold_by, t2.lat as brewery_lat, t2.lon as brewery_lon, t3.name, t3.lat as facility_lat, t3.lon as facility_lon FROM beers AS t1
                            INNER JOIN breweries AS t2
                            ON t1.made_by = t2.name
                            INNER JOIN (
                                SELECT * FROM pubs
                                UNION ALL
                                SELECT * FROM shops
                            ) AS t3
                            ON t1.sold_by = t3.name;""", con)
    indexAge = total[(total['facility_lat'] < 0.1) & (total['facility_lon'] < 0.1)].index
    total.drop(indexAge, inplace=True)
    geometry = [LineString([(row['facility_lon'], row['facility_lat']), (row['brewery_lon'], row['brewery_lat'])]) for idx, row in
                total.iterrows()]

    gdf = geopandas.GeoDataFrame(
        total, geometry=geometry, crs="EPSG:4326"
    )
    gdf.to_file(f'{shape_directory}/beers.shp')


def extract_database_to_shapefiles(database_path, shape_directory):
    extract_data_to_points(database_path, 'pubs', shape_directory)
    extract_data_to_points(database_path, 'shops', shape_directory)
    extract_data_to_points(database_path, 'breweries', shape_directory)
    extract_beer_lines(database_path, shape_directory)
    return shape_directory
