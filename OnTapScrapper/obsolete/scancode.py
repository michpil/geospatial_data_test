import re

def get_single_argument_function_info(function_line):
    new_line = function_line
    new_line = new_line.replace("def ", "").replace(":", "")

    name, protoargument = new_line.split("(")
    return {"name": name, "argument": protoargument[:-2]}


def get_function_code_from_file(function_name, filepath):
    file_text = open(filepath).read()
    function_index = file_text.find(function_name)
    two_new_lines_index = file_text.find("\n\n", function_index+1)
    return file_text[function_index-4:two_new_lines_index]


def get_functions_called_from_function(function_code):
    """
    works badly with multiple functions in one line like:
    pub_name = anchor.parent.text.strip().split("\n")[0].strip()
    """
    called_functions = []
    function_lines = function_code.split("\n")
    for line in function_lines[1:]:
        search = re.search(".+\(.*\)", line)
        if search:
            line_with_functions = search.group().strip()
            dotidx = line_with_functions.find(".") + 1
            if dotidx == 0:
                start_idx = line_with_functions.find(" = ") + 3
            else:
                start_idx = dotidx
            called_functions.append(line_with_functions[start_idx:line_with_functions.find("(")])
    return called_functions


def get_functions(python_file):
    with open(python_file) as f:
        all_function_info = []
        file_lines = f.readlines()
        for line in file_lines:
            if line.startswith("def"):
                func_info = get_single_argument_function_info(line)
                all_function_info.append(func_info)
        return all_function_info

all_f_info = get_functions("scrap_pub_site.py")
for f_i in all_f_info:
    print(f_i.get("name"), get_functions_called_from_function(get_function_code_from_file(f_i.get("name"), "scrap_pub_site.py")))
