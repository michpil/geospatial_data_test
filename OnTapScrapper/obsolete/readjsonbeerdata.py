import json
from datamodel import City, Brewery

def get_taps(filepath):
    with open(filepath, encoding='utf-8') as f:
        jsn = json.load(f)
        return jsn.get("taps")


def get_shops(filepath):
    with open(filepath, encoding='utf-8') as f:
        jsn = json.load(f)
        return jsn.get("shps")


def prepare_city_nodes(filepath):
    cities = []
    taps = get_taps(filepath)
    shops = get_shops(filepath)

    for tap in taps:
        ct = City(name=tap.get("city"))
        cities.append(ct)

    for shop in shops:
        ct = City(name=shop.get("city"))
        cities.append(ct)

    return set(cities)


from neomodel.contrib.spatial_properties import NeomodelPoint
filepath = "data/newjson20230614 224956.json"
taps = get_taps(filepath)
cities = prepare_city_nodes(filepath)





from neomodel import config
config.DATABASE_URL = 'bolt://neo4j:password@localhost:7687'
# for city in cities:
#     city.save()
lc= NeomodelPoint(44.123, 18.123)
b = Brewery(name="TESTLOC3", lat=44.123, lon=18.123, location=NeomodelPoint(lc), display_name="TSTSTSTS").save()
# b.location = lc
# b.save()
x=1