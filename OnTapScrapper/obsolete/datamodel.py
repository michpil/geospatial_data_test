from neomodel import config, StructuredNode, StringProperty, RelationshipTo, FloatProperty
from neomodel.contrib.spatial_properties import NeomodelPoint, PointProperty
import neomodel

class Brewery(StructuredNode):
    name = StringProperty(unique_index=True, required=True)
    lat = FloatProperty(required=False)
    lon = FloatProperty(required=False)
    location = PointProperty(crs="wgs-84")
    display_name = StringProperty(required=False)


class Beer(StructuredNode):
    name = StringProperty(unique_index=True, required=True)
    style = StringProperty(required=False)
    blg = FloatProperty(required=False)
    abv = FloatProperty(required=False)
    price_normal = FloatProperty(required=False)
    price_small = FloatProperty(required=False)

    brewery = RelationshipTo(Brewery, 'IS_FROM')


class City(StructuredNode):
    name = StringProperty(unique_index=True, required=True)

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        if not isinstance(other, type(self)): return NotImplemented
        return self.name == other.name


class Pub(StructuredNode):
    name = StringProperty(unique_index=True, required=True)
    lat = FloatProperty(required=False)
    lon = FloatProperty(required=False)
    location = PointProperty(crs="wgs-84")
    display_name = StringProperty(required=False)
    city = RelationshipTo(City, 'IS_FROM')

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        if not isinstance(other, type(self)): return NotImplemented
        return self.name == other.name


class Shop(StructuredNode):
    name = StringProperty(unique_index=True, required=True)
    lat = FloatProperty(required=False)
    lon = FloatProperty(required=False)
    location = PointProperty(crs="wgs-84")
    display_name = StringProperty(required=False)
    city = RelationshipTo(City, 'IS_FROM')

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        if not isinstance(other, type(self)): return NotImplemented
        return self.name == other.name
