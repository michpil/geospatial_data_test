import overpy

# Replace with your starting and ending latitude and longitude
start_lat = 54.5188900
start_lon = 18.5318800
end_lat = 54.3520500
end_lon = 18.6463700

api = overpy.Overpass()

# Define the query to find a walking route between two points
query = f"""
    [out:json];
    (
        node(around:1000,{start_lat},{start_lon}) -> .start;
        node(around:1000,{end_lat},{end_lon}) -> .end;
        way(bw.start.end)[highway=footway];
    );
    out body;
    >;
    out skel qt;
"""

result = api.query(query)

# Process the result
for way in result.ways:
    print("Way ID:", way.id)
    print("Nodes:", [(node.lat, node.lon) for node in way.nodes])