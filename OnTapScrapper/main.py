from extracttoshpfile import extract_database_to_shapefiles
from loadbeertodatabase import load_json_to_sqlite_db
from scrap_pub_site import get_beer_data, write_to_json

if __name__ == '__main__':
    rtlnk = "https://ontap.pl/"
    rec = get_beer_data(rtlnk)
    json_file_path = write_to_json(rec)
    database_path = load_json_to_sqlite_db(json_file_path, "beers.db")
    shp_dir = "data/shapes"
    extract_database_to_shapefiles(database_path, shp_dir)
