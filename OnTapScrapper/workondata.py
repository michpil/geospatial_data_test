def get_taps_tuples(file_json):
    taps_list = []
    taps = file_json.get('taps')

    for tap in taps:
        city = tap.get('city')
        name = tap.get('pub_name')
        lat = 0.0
        lon = 0.0
        if tap.get('geoinfo'):
            lat = tap.get('geoinfo').get('lat')
            lon = tap.get('geoinfo').get('lon')
        taps_list.append(f'("{city}", "{name}", "{round(float(lat), 6)}", "{round(float(lon), 6)}")')
        # CREATE TABLE IF NOT EXISTS pubs (id INTEGER PRIMARY KEY AUTOINCREMENT, city TEXT, name TEXT, lat REAL, lon REAL)

    return taps_list


def get_shop_tuples(file_json):
    taps_list = []
    shps = file_json.get('shps')

    for shp in shps:
        city = shp.get('city')
        name = shp.get('shop_name')
        lat = 0.0
        lon = 0.0
        if shp.get('geoinfo'):
            lat = shp.get('geoinfo').get('lat')
            lon = shp.get('geoinfo').get('lon')
        taps_list.append(f'("{city}", "{name}", "{round(float(lat), 6)}", "{round(float(lon), 6)}")')
        # CREATE TABLE IF NOT EXISTS shops (id INTEGER PRIMARY KEY AUTOINCREMENT, city TEXT, name TEXT, lat REAL, lon REAL)

    return taps_list


def get_breweries_tuples(file_json):
    breweries = []
    taps = file_json.get('taps')
    shops = file_json.get('shps')
    for tap in taps:
        beers = tap.get("beers")
        for beer in beers:
            brewery = beer.get("brewery")
            if brewery.get("geoinfo"):
                corrected_name = " ".join([word.capitalize() for word in brewery.get("name").split(" ")])
                lat = brewery.get("geoinfo").get('lat')
                lon = brewery.get("geoinfo").get('lon')

                if brewery.get("geoinfo").get('address').get('town') is not None:
                    city = brewery.get("geoinfo").get('address').get('town')
                elif brewery.get("geoinfo").get('address').get('city') is not None:
                    city = brewery.get("geoinfo").get('address').get('city')
                else:
                    city = brewery.get("geoinfo").get('address').get('village')

                breweries.append(f'("{city}", "{corrected_name}", "{round(float(lat), 6)}", "{round(float(lon), 6)}")')

    for shop in shops:
        beers = shop.get("beers")
        for beer in beers:
            brewery = beer.get("brewery")
            if brewery.get("geoinfo"):
                corrected_name = " ".join([word.capitalize() for word in brewery.get("name").split(" ")])
                lat = brewery.get("geoinfo").get('lat')
                lon = brewery.get("geoinfo").get('lon')
                if brewery.get("geoinfo").get('address').get('town') is not None:
                    city = brewery.get("geoinfo").get('address').get('town')
                elif brewery.get("geoinfo").get('address').get('city') is not None:
                    city = brewery.get("geoinfo").get('address').get('city')
                else:
                    city = brewery.get("geoinfo").get('address').get('village')
                breweries.append(f'("{city}", "{corrected_name}", "{round(float(lat), 6)}", "{round(float(lon), 6)}")')
                # CREATE TABLE IF NOT EXISTS breweries (id INTEGER PRIMARY KEY AUTOINCREMENT, city TEXT, name TEXT, lat REAL, lon REAL)

    return set(breweries)


def get_beers_tuples(file_json):
    beers_list = []
    taps = file_json.get('taps')
    shops = file_json.get('shps')

    for tap in taps:
        beers = tap.get("beers")
        for beer in beers:
            name = beer.get('name').replace('"', "'")
            brewery_name = beer.get('brewery').get('name')
            corrected_name = " ".join([word.lower().capitalize() for word in brewery_name.split(" ")])
            style = beer.get('style').replace('"', '')
            blg = beer.get('blg')
            abv = beer.get('abv')
            sold_in = tap.get('pub_name')
            beers_list.append(f'("{name}", "{corrected_name}", "{style}", "{blg}", "{abv}", "{sold_in}")')
            #CREATE TABLE IF NOT EXISTS beers (id INTEGER PRIMARY KEY AUTOINCREMENT, beer_name TEXT, made_by TEXT, style TEXT, blg TEXT, abv TEXT, sold_by TEXT)

    for shop in shops:
        beers = shop.get("beers")
        for beer in beers:
            name = beer.get('name').replace('"', "'")
            brewery_name = beer.get('brewery').get('name')
            corrected_name = " ".join([word.lower().capitalize() for word in brewery_name.split(" ")])
            style = beer.get('style').replace('"', '')
            blg = beer.get('blg')
            abv = beer.get('abv')
            sold_in = shop.get('shop_name')
            beers_list.append(f'("{name}", "{corrected_name}", "{style}", "{blg}", "{abv}", "{sold_in}")')

    return beers_list
