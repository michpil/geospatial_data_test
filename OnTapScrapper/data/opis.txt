Przygotowanie danych geolokalizowanych za pomocą Pythona i OpenStreetMap.

Dane związane z jakimś miejscem najlepiej oglądać na mapie. Żeby móc to zrobić, trzeba wykonać pewne kroki, które przypiszą lokalizację do rekordów.
Projekt OpenStreetMap udostępnia serwis Nominatim, którego użycie chciałem przedstawić do wykonania tego zadania.
A dalej to już z górki. Wystarczy tylko wrzucić do programu, który narysuje mapę i załatwione? Aż tak wygodnie nie jest, ale poradzimy sobie i z tym.
